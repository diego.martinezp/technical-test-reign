<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">

  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

The project is the result of the requirements of a technical exercise proposed by Reingn, which had as objective the creation of an api rest that responds to requests for news paged in groups of 5 and that can be filtered by attributes of the news.
For this we created this api rest with nest Js and mongo db.

- [Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Requirements

- [Docker](https://www.docker.com)
- [Docker-Compose](https://docs.docker.com/compose/)

## Installation and Setup

1. Clone the repo
   ```sh
   git clone https://gitlab.com/diego.martinezp/technical-test-reign.git
   ```
2. in the repo directory "technical-test-reign" build to create a docker image
   ```sh
   docker build -t technical-test-reign .
   ```
3. build docker-compose file to setup mongodb and api-rest service
   ```sh
   docker-compose build
   ```
4. run services rest-api in port 5000 and mongodb in port 27018
   ```sh
   docker-compose up
   ```

## Api Test with Postman

- The api collection requests are exported in the file "Api-test-reign.postman_collection.json"
- The api requests must be made in "http://localhost:5000/news/"
