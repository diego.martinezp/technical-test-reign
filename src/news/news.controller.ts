import {
  Controller,
  Get,
  Param,
  Res,
  Query,
  Delete,
  NotFoundException,
  HttpStatus,
} from '@nestjs/common';
import { query, Response } from 'express';
import { CreateNewsDTO } from './dto/news.dto';
import { NewsService } from './news.service';
import axios from 'axios';

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {
    //Setting up the initial news data, consulting the api provided in the specification
    const isCollectionEmpty = this.newsService
      .isCollectionEmpty()
      .then((isEmpty) => {
        if (isEmpty) {
          let seedNews: [];
          //get http request with axios
          axios(
            'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
          ).then((response) => {
            seedNews = response.data.hits;
            this.newsService.setSeedNews(seedNews);
          });
        }
      });
  }
  // Get news by page: /news/:page and query by news atributes /news/:page?<atribute1>=<value1>&<atribute2>=<value2>
  @Get(':page')
  async getNews(@Param('page') page, @Res() res: Response, @Query() query) {
    const news = await this.newsService.getNews(page, 5, query);
    //returning status and the result of query
    res.status(HttpStatus.OK).json({ news: news });
  }
  // delete news by ID: /news/delete?newsID=<id>
  @Delete('/delete')
  async deleteNews(@Res() res, @Query('newsID') newsID) {
    const newsDeleted = await this.newsService.deleteNewsById(newsID);
    //error if delete fails
    if (!newsDeleted) throw new NotFoundException('News does not exist!');
    //return status and the deleted news
    return res.status(HttpStatus.OK).json({
      message: 'News Deleted Successfully',
      newsDeleted,
    });
  }
}
