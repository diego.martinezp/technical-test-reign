import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { News } from './interfaces/news.interface';
import { CreateNewsDTO } from './dto/news.dto';

@Injectable()
export class NewsService {
  constructor(@InjectModel('News') readonly newsModel: Model<News>) {}
  //getting news by page and filtered by query if exits
  async getNews(
    page: number,
    nPerpage: number,
    query: object,
  ): Promise<News[]> {
    //filtering with query array and limit by 5 per page and return in order
    const news = await this.newsModel
      .find({ $and: [query] })
      .limit(nPerpage)
      .skip(page > 0 ? (page - 1) * nPerpage : 0);
    return news;
  }
  //setting up the initial news data in db  by a news array
  setSeedNews(createNewsDTOArray: CreateNewsDTO[]): boolean {
    createNewsDTOArray.forEach(async (createNewsDTO) => {
      const news = new this.newsModel(createNewsDTO);
      await news.save();
    });
    return true;
  }

  //consult if db news collection is empty for apply the initial configuration
  async isCollectionEmpty(): Promise<boolean> {
    const countNews = await this.newsModel.count();
    return countNews === 0 ? true : false;
  }

  //query for delete by propotioned id in the query
  async deleteNewsById(newsID: string): Promise<any> {
    const deletedNews = await this.newsModel.findByIdAndDelete(newsID);
    return deletedNews;
  }
}
